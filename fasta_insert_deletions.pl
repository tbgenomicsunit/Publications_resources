#fasta_insert_deletions is perl-based program used to insert random deletions in a target gene of a reference genome.
#Copyrigth (C) 2016 Alvaro Chiner Oms

#    fasta_insert_deletions.pl  is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    fasta_insert_deletions.pl is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    See <http://www.gnu.org/licenses/>.

#!/usr/bin/perl
use strict;
use File::Path::Expand;
use Bio::Tools::GFF;
use Bio::SeqIO;
use Clone 'clone';

#Start
my ( $ruta_fasta, $ruta_annot, @coord, $gene, $date );
my $tec = 0;

#read args
$ruta_fasta = expand_filename( $ARGV[0] );    #reference fasta path
$ruta_annot = expand_filename( $ARGV[1] );    #annotation path

for ( my $i = 2 ; $i < scalar(@ARGV) ; $i++ ) {
	if ( $ARGV[$i] eq "-g" ) { $gene = $ARGV[ $i + 1 ]; }    # target gene
}

#read fasta and annotation file
my $feature;
my $in_fasta = Bio::SeqIO->new(
	-file   => $ruta_fasta,
	-format => 'Fasta'
);
my $seq = $in_fasta->next_seq();
my $in_annot_file =
  Bio::Tools::GFF->new( -file => $ruta_annot, -gff_version => 3 );
while ( $feature = $in_annot_file->next_feature() ) {
	if ( grep { $gene eq $_ } $feature->get_tagset_values(qw(locus_tag)) )
	{    #look for the feature that matches gene name
		push( @coord, $feature->start() );    #gene start position
		push( @coord, $feature->end() );      #gene end position
	}
}

#introduce deletions
$date = localtime();
$date =~ s/\s/\_/g;
open( RES, ">Deletions_values_" . $date . ".txt" )
  || die "Sorry, but I can't create the file >Deletions_values" 
  . $date . ".txt";
print RES "File\tGene_affected\tOriginal_position\tDeletion_size\n";

$ruta_fasta =~ s/\..*//g;                     #remove fasta extension

my ( $rand_pos, $rand_del, $seq_modified, $sequence_nuc, $seqout );
for ( my $i = 0 ; $i < 5 ; $i++ ) {
	$seq_modified = clone($seq);

	#1 bp deletion
	$rand_pos     = $coord[0] + int( rand( $coord[1] - $coord[0] ) );
	$rand_del     = 1;
	$sequence_nuc = $seq->seq();
	substr( $sequence_nuc, $rand_pos, $rand_del, "" );
	$seq_modified->seq($sequence_nuc);
	$seq_modified->desc(
		$seq_modified->desc . "_$i" . "_1bp_deletion" . "$gene" );

	$seqout = Bio::SeqIO->new(
		-file => ">$ruta_fasta" . "_$i" . "_1bp_deletion" . "$gene" . ".fas",
		'-format' => 'Fasta'
	);
	$seqout->write_seq($seq_modified);

	print RES $ruta_fasta . "_" 
	  . $i
	  . "_1bp_deletion"
	  . $gene . "\t"
	  . $gene . "\t"
	  . $rand_pos . "\t"
	  . $rand_del . "\n";

	$seq_modified = clone($seq);

	#2-10 bp deletion
	$rand_pos     = $coord[0] + int( rand( $coord[1] - $coord[0] ) );
	$rand_del     = 2 + int( rand( 10 - 2 ) );
	$sequence_nuc = $seq->seq();
	substr( $sequence_nuc, $rand_pos, $rand_del, "" );
	$seq_modified->seq($sequence_nuc);
	$seq_modified->desc(
		$seq_modified->desc . "_$i" . "_2-10bp_deletion" . "$gene" );

	$seqout = Bio::SeqIO->new(
		-file => ">$ruta_fasta" . "_$i" . "_2-10bp_deletion" . "$gene" . ".fas",
		'-format' => 'Fasta'
	);
	$seqout->write_seq($seq_modified);

	print RES $ruta_fasta . "_" 
	  . $i
	  . "_2-10bp_deletion"
	  . $gene . "\t"
	  . $gene . "\t"
	  . $rand_pos . "\t"
	  . $rand_del . "\n";

	$seq_modified = clone($seq);

	#10-20 bp deletion
	$rand_pos     = $coord[0] + int( rand( $coord[1] - $coord[0] ) );
	$rand_del     = 10 + int( rand( 20 - 10 ) );
	$sequence_nuc = $seq->seq();
	substr( $sequence_nuc, $rand_pos, $rand_del, "" );
	$seq_modified->seq($sequence_nuc);
	$seq_modified->desc(
		$seq_modified->desc . "_$i" . "_10-20bp_deletion" . "$gene" );

	$seqout = Bio::SeqIO->new(
		-file => ">$ruta_fasta" . "_$i"
		  . "_10-20bp_deletion" . "$gene" . ".fas",
		'-format' => 'Fasta'
	);
	$seqout->write_seq($seq_modified);

	print RES $ruta_fasta . "_" 
	  . $i
	  . "_10-20bp_deletion"
	  . $gene . "\t"
	  . $gene . "\t"
	  . $rand_pos . "\t"
	  . $rand_del . "\n";

	$seq_modified = clone($seq);

	#20-40 bp deletion
	$rand_pos     = $coord[0] + int( rand( $coord[1] - $coord[0] ) );
	$rand_del     = 20 + int( rand( 40 - 20 ) );
	$sequence_nuc = $seq->seq();
	substr( $sequence_nuc, $rand_pos, $rand_del, "" );
	$seq_modified->seq($sequence_nuc);
	$seq_modified->desc(
		$seq_modified->desc . "_$i" . "_20-40bp_deletion" . "$gene" );

	$seqout = Bio::SeqIO->new(
		-file => ">$ruta_fasta" . "_$i"
		  . "_20-40bp_deletion" . "$gene" . ".fas",
		'-format' => 'Fasta'
	);
	$seqout->write_seq($seq_modified);

	print RES $ruta_fasta . "_" 
	  . $i
	  . "_20-40bp_deletion"
	  . $gene . "\t"
	  . $gene . "\t"
	  . $rand_pos . "\t"
	  . $rand_del . "\n";

	$seq_modified = clone($seq);

	#40-50 bp deletion
	$rand_pos     = $coord[0] + int( rand( $coord[1] - $coord[0] ) );
	$rand_del     = 40 + int( rand( 50 - 40 ) );
	$sequence_nuc = $seq->seq();
	substr( $sequence_nuc, $rand_pos, $rand_del, "" );
	$seq_modified->seq($sequence_nuc);
	$seq_modified->desc(
		$seq_modified->desc . "_$i" . "_40-50bp_deletion" . "$gene" );

	$seqout = Bio::SeqIO->new(
		-file => ">$ruta_fasta" . "_$i"
		  . "_40-50bp_deletion" . "$gene" . ".fas",
		'-format' => 'Fasta'
	);
	$seqout->write_seq($seq_modified);

	print RES $ruta_fasta . "_" 
	  . $i
	  . "_40-50bp_deletion"
	  . $gene . "\t"
	  . $gene . "\t"
	  . $rand_pos . "\t"
	  . $rand_del . "\n";

	$seq_modified = clone($seq);

	#50-60 bp deletion
	$rand_pos     = $coord[0] + int( rand( $coord[1] - $coord[0] ) );
	$rand_del     = 50 + int( rand( 60 - 50 ) );
	$sequence_nuc = $seq->seq();
	substr( $sequence_nuc, $rand_pos, $rand_del, "" );
	$seq_modified->seq($sequence_nuc);
	$seq_modified->desc(
		$seq_modified->desc . "_$i" . "_50-60bp_deletion" . "$gene" );

	$seqout = Bio::SeqIO->new(
		-file => ">$ruta_fasta" . "_$i"
		  . "_50-60bp_deletion" . "$gene" . ".fas",
		'-format' => 'Fasta'
	);
	$seqout->write_seq($seq_modified);

	print RES $ruta_fasta . "_" 
	  . $i
	  . "_50-60bp_deletion"
	  . $gene . "\t"
	  . $gene . "\t"
	  . $rand_pos . "\t"
	  . $rand_del . "\n";

	$seq_modified = clone($seq);

	#60-100 bp deletion
	$rand_pos     = $coord[0] + int( rand( $coord[1] - $coord[0] ) );
	$rand_del     = 60 + int( rand( 100 - 60 ) );
	$sequence_nuc = $seq->seq();
	substr( $sequence_nuc, $rand_pos, $rand_del, "" );
	$seq_modified->seq($sequence_nuc);
	$seq_modified->desc( $seq_modified->desc . "_$i" . "_60-100bp_deletion" );

	$seqout = Bio::SeqIO->new(
		-file => ">$ruta_fasta" . "_$i"
		  . "_60-100bp_deletion" . "$gene" . ".fas",
		'-format' => 'Fasta'
	);
	$seqout->write_seq($seq_modified);

	print RES $ruta_fasta . "_" 
	  . $i
	  . "_60-100bp_deletion"
	  . $gene . "\t"
	  . $gene . "\t"
	  . $rand_pos . "\t"
	  . $rand_del . "\n";
}

close(RES);
