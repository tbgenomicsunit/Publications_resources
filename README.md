fasta_insert_deletions.pl
-------------------------

It creates alternative fasta files with 7 different deletion ranges a target gene.
It takes as an input a fasta file containing a genome, an annotation file and a target gene. It generates 5 different fasta files for each deletion range (1 bp, 2-10 bp, 10-20 bp, 20-40 bp, 40-50 bp, 50-60 bp and 60-100 bp). The new fasta files are placed in the same folder containing the reference genome. Also a .txt file with a summary of the new files created is generated in the script folder. The following perl modules are necessary to run the code:

* File::Path::Expand
* BioPerl
* Clone

Example:
```
> perl fasta_insert_deletions.pl H37Rv.fas H37Rv.gff -g Rv0757
```
In conjunction with the [ART tool](https://www.ncbi.nlm.nih.gov/pubmed/22199392) allows you to create simulated fastq files with random deletions in specific regions that could be used to test NGS analysis pipelines. It was used in [Ezewudo *et al.* 2018](https://www.nature.com/articles/s41598-018-33731-1).

*****************

MTB_ancestor.fas
----------------

The *Mycobacterium tuberculosis* complex most likely common ancestor derived as described in [Comas *et al.* 2010.](https://www.nature.com/articles/ng.590)

****************

regulatory_network
----------------

We have derived a new *Mycobacterium tuberculosis* regulatory network based on experimental data and statistical approaches [Chiner-Oms *et al.* 2018](https://www.nature.com/articles/s41598-018-22237-5).

****************

pN/pS trajectories
----------------

Dataset S7 from [Gene Evolutionary Trajectories in M. tuberculosis Reveal Temporal Signs of Selection](https://www.biorxiv.org/content/10.1101/2021.07.15.452434v1)
